@extends('layout/main')

@section('title', 'Universitas')
    

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                    <h1 class="mt-1">Universitas Brawijaya</h1>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat,
                      corporis nisi vitae asperiores voluptas delectus temporibus suscipit
                      nesciunt consequuntur corrupti, praesentium earum incidunt ipsam
                      veniam minus? Reiciendis, alias error! Voluptatem? Lorem ipsum, dolor
                      sit amet consectetur adipisicing elit. Laborum cum, architecto, id
                      blanditiis animi autem similique et aspernatur in omnis natus quasi
                      hic maxime officia quidem enim, repellat debitis consequatur?
                    </p>
                    
                    <h1 class="mt-1">Daftar Mahasiswa</h1>
                    
                    <table class="table">
                        <thead class="table-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Nama</th>
                          <th scope="col">NIM</th>
                          <th scope="col">Email</th>
                          <th scope="col">Jurusan</th>
                          <th scope="col">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($mahasiswa as $mhs)

                          <tr>
                              <th scope="row">110987</th>
                              <td>{{ $mhs->nama }}</td>
                              <td>{{ $mhs->nim }}</td>
                              <td>{{ $mhs->email }}</td>
                              <td>{{ $mhs->jurusan }}</td>
                              <td>
                                  <a href="" class="badge bg-success">edit</a>
                                  <a href="" class="badge bg-danger">delete</a>
                                  
                              </td>
                          </tr>
                        @endforeach

                        </tbody>
                      </table>

            </div>
        </div>
    </div>
@endsection
    