@extends('layout/main')

@section('title', 'Home')
    

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                    <h1 class="mt-1">Tugas Laravel</h1> 
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat,
                        corporis nisi vitae asperiores voluptas delectus temporibus suscipit
                        nesciunt consequuntur corrupti, praesentium earum incidunt ipsam
                        veniam minus? Reiciendis, alias error! Voluptatem? Lorem ipsum, dolor
                        sit amet consectetur adipisicing elit. Laborum cum, architecto, id
                        blanditiis animi autem similique et aspernatur in omnis natus quasi
                        hic maxime officia quidem enim, repellat debitis consequatur?
                      </p>
            </div>
        </div>
    </div>
@endsection
    